<?php
//define('URL_MAIN', 'https://dbuycommerce.mixstudioweb.com.br');
define('URL_MAIN', 'http://localhost:8080/dbuycommerce.com.br');
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- The above 4 meta tags *must* come first in the head; any other head content must come *after* these tags-->
    <!-- Title-->
    <title>DBuy Commerce - Aumente sua receita vendendo pela internet</title>
    <!-- Favicon-->
    <link rel="icon" href="<?php echo URL_MAIN; ?>/assets/img/core-img/favicon.ico">
    <!-- Stylesheet-->
    <link rel="stylesheet" href="<?php echo URL_MAIN; ?>/assets/css/style.css?v=<?php rand(); ?>">
</head>maps
<body>
<!-- Header Area-->
<header class="header_area">
    <div class="main_header_area">
        <div class="container">
            <div class="classy-nav-container breakpoint-off">
                <nav class="classy-navbar justify-content-between" id="aplandNav">
                    <!-- Logo--><a class="nav-brand" href="index.html"><img src="<?php echo URL_MAIN; ?>/assets/img/core-img/dbuy.png" alt=""></a>
                    <!-- Navbar Toggler-->
                    <div class="classy-navbar-toggler"><span class="navbarToggler"><span></span><span></span><span></span></span></div>
                    <!-- Menu-->
                    <div class="classy-menu">
                        <!-- Close Button-->
                        <div class="classycloseIcon">
                            <div class="cross-wrap"><span class="top"></span><span class="bottom"></span></div>
                        </div>
                        <!-- Nav-->
                        <div class="classynav">
                            <ul id="corenav">
                                <li><a href="#features">Ferramentas</a></li>
                                <li><a href="#vendas">Manual de vendas</a></li>
                                <li><a href="#init">Como começar</a></li>
                                <li><a href="#server">Servidor de hospedagem</a></li>
                            </ul>
                            <!-- Login Button-->
                            <div class="login-btn-area ml-5 mt-5 mt-lg-0">
                                <a class="btn apland-btn" href="#">Eu Quero!</a>
                            </div>
                        </div>
                    </div>
                </nav>
            </div>
        </div>
    </div>
</header>

<!-- Welcome Area-->
<section class="hero-barishal welcome_area" id="home">
    <!-- Background Shape-->
    <div class="background-shapes">
        <div class="box1"></div>
        <div class="box2"></div>
        <div class="box3"></div>
        <div class="dot1"></div>
        <div class="dot2"></div>
        <div class="dot3"></div>
        <div class="dot4"></div>
        <div class="heart1"><i class="lni-heart"></i></div>
        <div class="heart2"><i class="i lni-heart"></i></div>
        <div class="circle1"></div>
        <div class="circle2"></div>
    </div>
    <div class="container h-100">
        <div class="row h-100 justify-content-between align-items-center">
            <div class="col-12 col-md-6">
                <div class="welcome_text_area">
                    <h2 class="wow fadeInUp" data-wow-delay="0.2s">Aumente sua receita <span> vendendo </span> pela internet! <span></span></h2>
                    <h5 class="wow fadeInUp" data-wow-delay="0.3s">Deseja vender pela internet? <br /> Mas, não sabe por onde começar? <br />
                        Cuidamos de toda a tecnologia! <br /> Se preocupe apenas com a logística! </h5>
                    <a class="btn apland-btn mt-30 wow fadeInUp" href="#" data-wow-delay="0.4s"> Teste 30 dias grátis! </a>
                </div>
            </div>
            <div class="col-10 col-sm-8 col-md-5">
                <div class="welcome_area_thumb text-center wow fadeInUp" data-wow-delay="0.2s"><img src="<?php echo URL_MAIN; ?>/assets/img/bg-img/hero-2.png" alt=""></div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="welcome-border"></div>
    </div>
</section>

<!-- Modulos -->
<section class="about_area section_padding_130" id="features">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 col-sm-8 col-lg-6">
                <!-- Section Heading-->
                <div class="section_heading text-center wow fadeInUp" data-wow-delay="0.2s">
                    <h6> Dbuy Commerce </h6>
                    <h3>Conheça as <span>ferramentas</span> que vão otimizar suas vendas.</h3>
                    <div class="line"></div>
                </div>
            </div>
        </div>
        <div class="row justify-content-center align-items-center">
            <div class="col-12 col-sm-8 col-md-6 col-lg-4">
                <div class="about_product_discription">
                    <div class="row">
                        <!-- Single About Area-->
                        <div class="col-12">
                            <div class="single_about_part wow fadeInUp" data-wow-delay="0.2s">
                                <div class="feature_icon"><i class="lni-travel"></i></div>
                                <h6>Formas de envio</h6>
                                <p>Correios, Retirar pessoalmente, Motoboy e Transportadora.</p>
                            </div>
                        </div>
                        <!-- Single About Area-->
                        <div class="col-12">
                            <div class="single_about_part wow fadeInUp" data-wow-delay="0.3s">
                                <div class="feature_icon"><i class="lni-money-protection"></i></div>
                                <h6>Formas de pagamento</h6>
                                <p>Checkout Transparente, Mercado Pago, PayPal, PagSeguro, Boleto registrado, Cartão de débito ou crédito.</p>
                            </div>
                        </div>
                        <!-- Single About Area-->
                        <div class="col-12">
                            <div class="single_about_part wow fadeInUp" data-wow-delay="0.4s">
                                <div class="feature_icon"><i class="lni-cloudnetwork"></i></div>
                                <h6>Marketplaces e Hubs</h6>
                                <p>Mais de 20 Marketplaces e Hubs, entre eles Mercado Livre, Magalu, Carrefour e Amazon.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Product Thumb Area-->
            <div class="col-12 col-md-8 col-lg-4">
                <div class="about_product_thumb text-center my-5 my-lg-0"><img src="<?php echo URL_MAIN; ?>/assets/img/bg-img/features-img.png" alt=""></div>
            </div>
            <div class="col-12 col-sm-8 col-md-6 col-lg-4">
                <div class="about_product_discription">
                    <div class="row">
                        <!-- Single About Area-->
                        <div class="col-12">
                            <div class="single_about_part wow fadeInUp" data-wow-delay="0.2s">
                                <div class="feature_icon"><i class="lni-seo-monitoring"></i></div>
                                <h6>Otimização nas vendas</h6>
                                <p>Módulo SEO Dbuy, Schema Webmasters, AMP - Accelerated Mobile Pages, PWA - Progressive Web App.</p>
                            </div>
                        </div>
                        <!-- Single About Area-->
                        <div class="col-12">
                            <div class="single_about_part wow fadeInUp" data-wow-delay="0.3s">
                                <div class="feature_icon"><i class="lni-code"></i></div>
                                <h6>Integrações</h6>
                                <p>Google Analytics, Google Webmasters, Google Adwords, Webmasters Bing e Chat Jivo.</p>
                            </div>
                        </div>
                        <!-- Single About Area-->
                        <div class="col-12">
                            <div class="single_about_part wow fadeInUp" data-wow-delay="0.4s">
                                <div class="feature_icon"><i class="lni-protection"></i></div>
                                <h6>Servidor de hospedagem</h6>
                                <p>Certificado SSL, IP dedicado, Integração Cloudflare núvem e 3 e-mails com espaço ilimitado.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Video Area-->
<section class="video_area" id="vendas">
    <!-- Video Top Background-->
    <div class="video-top-thumbnail"><img src="<?php echo URL_MAIN; ?>/assets/img/core-img/video-top.png" alt=""></div>
    <div class="container h-100">
        <div class="row h-100 align-items-center">
            <div class="col-12">
                <div class="video_area_text text-center">
                    <h2 class="wow fadeInUp" data-wow-delay="0.2s">Manual de vendas</h2>
                    <p class="wow fadeInUp" data-wow-delay="0.3s">Tenha total acesso ao nosso manual de vendas progressiva!</p>
                    <a class="video_btn wow fadeInUp" href="https://www.youtube.com/watch?v=YLtFGWVWiGo" data-wow-delay="0.4s"><i class="lni-play"></i><span class="video-sonar"></span></a>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Work Process Area-->
<section class="work_process_area section_padding_130_80" id="init">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 col-sm-8 col-lg-6">
                <!-- Section Heading-->
                <div class="section_heading text-center">
                    <h3>Como faço para <span>começar?</span></h3>
                    <p>Em apenas 3 simples passos você irá começar a vender pela internet.</p>
                    <div class="line"></div>
                </div>
            </div>
        </div>
        <div class="row justify-content-between">
            <!-- Single Work Step-->
            <div class="col-12 col-sm-4 col-md-3">
                <div class="single_work_step">
                    <div class="step-icon"><i class="lni-text-format"></i></div>
                    <h5>Dados cadastrais</h5>
                    <p>Informar no formulário os dados requeridos.</p>
                </div>
            </div>
            <!-- Single Work Step-->
            <div class="col-12 col-sm-4 col-md-3">
                <div class="single_work_step">
                    <div class="step-icon"><i class="lni-paint-bucket"></i></div>
                    <h5>Configuração</h5>
                    <p>Não se preocupe com isso! Fazemos toda a configuração da sua loja!</p>
                </div>
            </div>
            <!-- Single Work Step-->
            <div class="col-12 col-sm-4 col-md-3">
                <div class="single_work_step">
                    <div class="step-icon"><i class="lni-bolt-alt"></i></div>
                    <h5>Comece a vender</h5>
                    <p>Em apenas 5 dias, sua loja estará pronta para vender! <br /> Siga o passo-a-passo do nosso manual de vendas. <br /> Boas vendas!</p>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="container">
    <div class="border-top"></div>
</div>

<!-- Layouts -->
<div class="app_screenshot_area section_padding_130_0">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 col-sm-8 col-lg-6">
                <!-- Section Heading-->
                <div class="section_heading text-center">
                    <h3>Conheça os <span>layouts<br></span></h3>
                    <p>Layouts limpos e responsivos que abrem perfeitamente nos dispositivos móveis, tendo como objetivo dar destaque nos produtos oferecidos.</p>
                    <div class="line"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- Screenshot Slider-->
    <div class="app_screenshots_area">
        <div class="container">
            <div class="app_screenshots owl-carousel">
                <!-- Single Screenshot-->
                <div class="single_screenshot"><img src="<?php echo URL_MAIN; ?>/assets/img/screenshot/1.jpg" alt=""></div>
                <!-- Single Screenshot-->
                <div class="single_screenshot"><img src="<?php echo URL_MAIN; ?>/assets/img/screenshot/2.jpg" alt=""></div>
                <!-- Single Screenshot-->
                <div class="single_screenshot"><img src="<?php echo URL_MAIN; ?>/assets/img/screenshot/3.jpg" alt=""></div>
                <!-- Single Screenshot-->
                <div class="single_screenshot"><img src="<?php echo URL_MAIN; ?>/assets/img/screenshot/4.jpg" alt=""></div>
                <!-- Single Screenshot-->
                <div class="single_screenshot"><img src="<?php echo URL_MAIN; ?>/assets/img/screenshot/5.jpg" alt=""></div>
                <!-- Single Screenshot-->
                <div class="single_screenshot"><img src="<?php echo URL_MAIN; ?>/assets/img/screenshot/6.jpg" alt=""></div>
                <!-- Single Screenshot-->
                <div class="single_screenshot"><img src="<?php echo URL_MAIN; ?>/assets/img/screenshot/8.jpg" alt=""></div>
                <!-- Single Screenshot-->
                <div class="single_screenshot"><img src="<?php echo URL_MAIN; ?>/assets/img/screenshot/9.jpg" alt=""></div>
                <!-- Single Screenshot-->
                <div class="single_screenshot"><img src="<?php echo URL_MAIN; ?>/assets/img/screenshot/10.jpg" alt=""></div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <!-- CTA Thumbnail-->
                <div class="cta-thumbnail section_padding_130_0"><img class="w-100" src="<?php echo URL_MAIN; ?>/assets/img/bg-img/cta.jpg" alt=""></div>
            </div>
        </div>
    </div>
</div>

<!-- Testimonial Area-->
<section class="testimonial_area section_padding_130">
    <!-- Testimonial Top Thumbnail-->
    <div class="testimonial-top-thumbnail"><img src="<?php echo URL_MAIN; ?>/assets/img/core-img/testimonial-top.png" alt=""></div>
    <!-- Testimonial Bottom Thumbnail-->
    <div class="testimonial-bottom-thumbnail"><img src="<?php echo URL_MAIN; ?>/assets/img/core-img/testimonial-bottom.png" alt=""></div>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 col-sm-8 col-lg-6">
                <!-- Section Heading-->
                <div class="section_heading white text-center">
                    <h3>Depoimentos de nossos Clientes</h3>
                    <p>Saiba o que nossos clientes estão falando sobre nós.</p>
                    <div class="line"></div>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-10 col-md-9 col-lg-7">
                <div class="card border-0 p-4 p-sm-5 testimonials owl-carousel">
                    <!-- Single Testimonial Area-->
                    <div class="single_testimonial_area text-center">
                        <div class="testimonial_author_thumb"><img src="<?php echo URL_MAIN; ?>/assets/img/advisor-img/testimonial-1.jpg" alt=""></div>
                        <div class="testimonial_text">
                            <p>Estou muito feliz com o resultado que estou obtendo com a loja virtual!</p>
                            <div class="rating"><i class="lni-star-filled"></i><i class="lni-star-filled"></i><i class="lni-star-filled"></i><i class="lni-star-filled"></i><i class="lni-star-filled"></i></div>
                            <div class="testimonial_author_name">
                                <h5>Valdir Alves</h5>
                                <h6>Empresário &amp; Tapeçaria Esporte</h6>
                            </div>
                        </div>
                    </div>
                    <!-- Single Testimonial Area-->
                    <div class="single_testimonial_area text-center">
                        <div class="testimonial_author_thumb"><img src="<?php echo URL_MAIN; ?>/assets/img/advisor-img/testimonial-2.jpg" alt=""></div>
                        <div class="testimonial_text">
                            <p>Estou muito satisfeita! Suporte rápido e de qualidade, o <strong>Manual de vendas</strong> está me ajudando demais!</p>
                            <div class="rating"><i class="lni-star-filled"></i><i class="lni-star-filled"></i><i class="lni-star-filled"></i><i class="lni-star-filled"></i><i class="lni-star-filled"></i></div>
                            <div class="testimonial_author_name">
                                <h5>Mãe do Vinícius</h5>
                                <h6>Empresária &amp; Toner já!</h6>
                            </div>
                        </div>
                    </div>
                    <!-- Single Testimonial Area-->
                    <div class="single_testimonial_area text-center">
                        <div class="testimonial_author_thumb"><img src="<?php echo URL_MAIN; ?>/assets/img/advisor-img/testimonial-1.jpg" alt=""></div>
                        <div class="testimonial_text">
                            <p>Sempre quis ter outro canal de venda além das minhas lojas física, a <strong>Dbuy Commerce</strong> está me ajudando muito nessa nova missão.</p>
                            <div class="rating"><i class="lni-star-filled"></i><i class="lni-star-filled"></i><i class="lni-star-filled"></i><i class="lni-star-filled"></i><i class="lni-star-filled"></i></div>
                            <div class="testimonial_author_name">
                                <h5>Fernando Oliver</h5>
                                <h6>Empresário &amp; Oliver Suplementos</h6>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Servidor de hospedagem -->
<section class="main_features_area bg-gray section_padding_130" id="server" style="background-color:transparent;">
    <div class="container">
        <div class="row align-items-center justify-content-between">
            <div class="col-12 col-sm-10 col-md-6">
                <!-- Section Heading-->
                <div class="section_heading wow fadeInUp" data-wow-delay="0.2s">
                    <h6>Servidor de Hospedagem</h6>
                    <h3>Conheça toda tecnologia de alta performance</h3>
                    <div class="line mr-auto ml-0"></div>
                </div>
                <!-- Single Item-->
                <div class="single_benifits d-flex wow fadeInUp" data-wow-delay="0.2s">
                    <div class="icon_box mr-30"><i class="lni-check-mark-circle"></i></div>
                    <div class="single_benifits_text">
                        <h5>Velocidade</h5>
                        <p>Extraordinário tempo de carregamento graças a nossa baixa latência! </p>
                    </div>
                </div>
                <!-- Single Item-->
                <div class="single_benifits d-flex wow fadeInUp" data-wow-delay="0.2s">
                    <div class="icon_box mr-30"><i class="lni-check-mark-circle"></i></div>
                    <div class="single_benifits_text">
                        <h5>Proteção</h5>
                        <p>Contra ataques de hackers e ataques DDoS.</p>
                    </div>
                </div>
                <!-- Single Item-->
                <div class="single_benifits d-flex wow fadeInUp" data-wow-delay="0.2s">
                    <div class="icon_box mr-30"><i class="lni-check-mark-circle"></i></div>
                    <div class="single_benifits_text">
                        <h5>Máquinas dedicada</h5>
                        <p>Máquinas próprias instaladas em pontos estratégicos do Brasil. Manutenção do hardware e software é garantida!</p>
                    </div>
                </div>
                <!-- Single Item-->
                <div class="single_benifits d-flex wow fadeInUp" data-wow-delay="0.2s">
                    <div class="icon_box mr-30"><i class="lni-check-mark-circle"></i></div>
                    <div class="single_benifits_text">
                        <h5>Suporte</h5>
                        <p>Suporte técnico 24 horas!</p>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-6 col-lg-5">
                <div class="main_features_thumbnail mt-5 mt-md-0"><img src="<?php echo URL_MAIN; ?>/assets/img/bg-img/hero-4.png" alt=""></div>
            </div>
        </div>
    </div>
</section>

<!-- Price and Plans Area-->
<section class="price_plan_area section_padding_130_80" id="pricing">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 col-sm-8 col-lg-6">
                <!-- Section Heading-->
                <div class="section_heading text-center wow fadeInUp" data-wow-delay="0.2s">
                    <h6>Dbuy Commerce</h6>
                    <h3>Pronto para pensar à frente?</h3>
                    <p>Empresários de sucesso estão sempre buscando novos meios para
                        aumentar seu patrimônio, seu negócio. <br />

                        As <strong>lojas físicas</strong> estão se transformando de <strong>ponto de venda para
                            ponto de experiência</strong>, isso aumenta o peso da estratégia digital.</p>
                    <div class="line"></div>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-12 col-sm-8 col-md-6 col-lg-4">
                <div class="single_price_plan active wow fadeInUp" data-wow-delay="0.2s">
                    <!-- Side Shape-->
                    <div class="side-shape"><img src="<?php echo URL_MAIN; ?>/assets/img/core-img/popular.png" alt=""></div>
                    <div class="title"><span style="background-color: #6d6d6d;width:60%;">Parceiros <img src="<?php echo URL_MAIN; ?>/assets/img/trokz.png" alt=""></span>
                        <p>Válido até 20 de Abril</p>
                        <div class="line"></div>
                    </div>
                    <div class="price">
                        <h4>R$ 2.000,00 <small>100% Permuta!</small></h4>
                    </div>
                    <div class="description">
                        <p><i class="lni-check-mark-circle"></i>Configuração layout</p>
                        <p><i class="lni-check-mark-circle"></i>Todos os módulos de pagamento, envio, marketplaces, SEO e integrações</p>
                        <p><i class="lni-check-mark-circle"></i>Acesso ao manual de vendas</p>
                        <p><i class="lni-check-mark-circle"></i>Servidor alta performance</p>
                        <p><i class="lni-check-mark-circle"></i>2 E-mails profissionais ilimitados</p>
                        <p><i class="lni-check-mark-circle"></i>Suporte 24 horas</p>
                        <p style="color:green;"><i class="lni-cake"></i>Ganhe!</p>
                        <p><i class="lni-check-mark-circle"></i>Teste: 30 dias grátis</p>
                        <p><i class="lni-check-mark-circle"></i>Cadastro dos 20 primeiros produtos</p>
                        <p><i class="lni-check-mark-circle"></i>Criação 2 banners promocionais</p>
                    </div>
                    <div class="button"><a class="btn apland-btn" href="#">Eu Quero!</a></div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Desenvolvimento contínuo -->
<section class="organize-your-sales section_padding_130_80">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 col-sm-8 col-lg-6">
                <!-- Section Heading-->
                <div class="section_heading text-center">
                    <h6>Dbuy Commerce</h6>
                    <h3>Desenvolvimento contínuo</h3>
                    <div class="line"></div>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-12 col-lg-10">
                <div class="organize-your-sales-content text-center mb-5">
                    <p>Sempre se mantendo à frente no mercado de e-commerce, nossa Equipe estará sempre desenvolvendo e otimizando o código fonte da loja virtual <strong>Dbuy Commerce</strong>,
                        buscando obter a melhor experiência de usuários, resultando em vendas para nossos clientes. <br />
                        Conceito, alta performance e perspectivas revolucionárias fazem parte do nosso trabalho!</p>
                    <br />
                    <br />
                    <br />
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Message Now Area-->
<div class="message_now_area bg-gray wow fadeInUp" data-wow-delay="0.5s">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="apland-contact-form">
                    <!-- Section Title-->
                    <div class="message_title">
                        <div class="section_heading">
                            <h6>Dbuy Commerce</h6>
                            <h3>Ficou alguma dúvida?</h3>
                            <p>Caso tenha ficado alguma dúvida, basta preencher o formulário abaixo, teremos o prazer em conversar.</p>
                            <div class="line ml-0"></div>
                        </div>
                    </div>
                    <!-- Contact Form-->
                    <div class="contact_from">
                        <form id="main_contact_form" action="" method="post">
                            <div class="contact_input_area">
                                <div id="success_fail_info"></div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group mb-30">
                                            <input class="form-control" id="name" type="text" name="name" placeholder="Seu Nome" required>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group mb-30">
                                            <input class="form-control" id="email" type="email" name="email" placeholder="Seu E-mail" required>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group mb-30">
                                            <input class="form-control" id="subject" type="text" name="subject" placeholder="Assunto" required>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group mb-30">
                                            <input class="form-control" id="number" type="text" name="number" placeholder="Seu Telefone" required>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="form-group mb-30">
                                            <textarea class="form-control" id="message" name="message" cols="30" rows="10" placeholder="Sua Mensagem" required></textarea>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <button class="btn apland-btn" type="submit">ENVIAR AGORA</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- jQuery(necessary for all JavaScript plugins)-->
<script src="<?php echo URL_MAIN; ?>/assets/js/jquery.min.js"></script>
<script src="<?php echo URL_MAIN; ?>/assets/js/popper.min.js"></script>
<script src="<?php echo URL_MAIN; ?>/assets/js/bootstrap.min.js"></script>
<script src="<?php echo URL_MAIN; ?>/assets/js/owl.carousel.min.js"></script>
<script src="<?php echo URL_MAIN; ?>/assets/js/waypoints.min.js"></script>
<script src="<?php echo URL_MAIN; ?>/assets/js/jquery.easing.min.js"></script>
<script src="<?php echo URL_MAIN; ?>/assets/js/default/classy-nav.min.js"></script>
<script src="<?php echo URL_MAIN; ?>/assets/js/default/sticky.js"></script>
<script src="<?php echo URL_MAIN; ?>/assets/js/default/mail.js"></script>
<script src="<?php echo URL_MAIN; ?>/assets/js/default/scrollup.min.js"></script>
<script src="<?php echo URL_MAIN; ?>/assets/js/default/one-page-nav.js"></script>
<script src="<?php echo URL_MAIN; ?>/assets/js/jarallax.min.js"></script>
<script src="<?php echo URL_MAIN; ?>/assets/js/jarallax-video.min.js"></script>
<script src="<?php echo URL_MAIN; ?>/assets/js/jquery.counterup.min.js"></script>
<script src="<?php echo URL_MAIN; ?>/assets/js/jquery.countdown.min.js"></script>
<script src="<?php echo URL_MAIN; ?>/assets/js/jquery.magnific-popup.min.js"></script>
<script src="<?php echo URL_MAIN; ?>/assets/js/wow.min.js"></script>
<script src="<?php echo URL_MAIN; ?>/assets/js/default/active.js"></script>
</body>
</html>